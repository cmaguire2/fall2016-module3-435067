<!DOCTYPE html>
<body>
<?php session_start();
?>
<form method="POST">
<textarea name ="comment"></textarea>
<input type = "submit" name="submitComment" value="submit"/>
</form>
<a href="guide.php">Back</a><br>
</body>
    
<?php
require "Database.php";
if(!isset($_POST['submitComment'])){
	if(isset($_GET["post_id"])){
		$_SESSION['post_id'] = (int) $_GET["post_id"];
	}
	elseif (isset($_POST["post_id"])){
		$_SESSION['post_id'] = (int) $_POST["post_id"];
	}
	loadComment();
}
function loadComment() {
	require "Database.php";
	$stmt2 = $mysqli->prepare("SELECT u.username, u.user_id, c.comment, c.comment_id FROM comments c JOIN users u ON c.user_id=u.user_id WHERE c.story_id = ?");
	if(!$stmt2){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt2->bind_param ('i', $_SESSION['post_id']);
	$stmt2->execute();
	$stmt2->bind_result($username, $user_id, $comment, $comment_id);
	
	while($stmt2->fetch()){
		if($user_id == $_SESSION['user']){		
			echo $username.": ".$comment.'<br>';
	      		echo '<form method = "POST" action = "editComment.php">'; 
	      		echo '<input type="hidden" name="comment_id" value ="'.$comment_id.'">';
	      		echo '<button type = "submit" name = "editComment" value = "edit">Edit</button>';
	      		echo '</form>';
	      		echo '<form method = "POST" action = "deleteComment.php">'; 
	      		echo '<input type="hidden" name="comment_id" value ="'.$comment_id.'">';
			echo '<button type = "submit" name = "deleteComment" value = "delete">Delete</button>';
	      		echo '</form>';
		}
		else{
			echo $username.": ".$comment.'<br>';
		}
	}
}

if(isset($_POST['submitComment'])){
	$post_id = $_SESSION['post_id'];
	$commentContent = $_POST['comment'];
        if($commentContent == "") {
            echo "No BLANK COMMENTS allowed";
        }
        else {
            $stmt = $mysqli->prepare("INSERT INTO comments (user_id, story_id, comment) VALUES (?, ?, ?)");
            if(!$stmt){
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
            }
	    $stmt->bind_param ('iis', $_SESSION['user'], $post_id, $commentContent);
            $stmt->execute();
            printf("%d", $mysqli->affected_rows);
            $stmt->close();
        }

	loadComment();
}

?>
</html>
