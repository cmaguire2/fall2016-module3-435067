<!DOCTYPE html>

<form method="POST">
<textarea name ="deletePost"></textarea>
<input type = "submit" name="deletePost" value="submit"/>
</form>
</body>

<?php

session_start();
$story_id = (int) $_POST['post_id'];

require "Database.php";

$stmt = $mysqli->prepare("delete FROM stories WHERE story_id = ?");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
 
$stmt->bind_param('i', $story_id);
 
$stmt->execute();
 
$stmt->close();
header("Location: guide.php");

?>

