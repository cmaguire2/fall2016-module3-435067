<!DOCTYPE html>

<form method="POST">
<textarea name ="editComment"></textarea>
<input type = "submit" name="submitNewComment" value="submit"/>
</form>
</body>

<?php

session_start();
if(!isset($_POST['submitNewComment'])){
	$_SESSION['comment_id']=$_POST['comment_id'];
}
if(isset($_POST['submitNewComment'])){

$editComment = $_POST['editComment'];

require "Database.php";

$stmt = $mysqli->prepare("update comments SET comment = ? where comment_id = ?");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
 
$stmt->bind_param('si', $editComment, $_SESSION['comment_id']);
 
$stmt->execute();
 
$stmt->close();
$commentHistory = sprintf("Comments.php?post_id=%s", $_SESSION['post_id']);
header("Location: $commentHistory");

}


 
?>
