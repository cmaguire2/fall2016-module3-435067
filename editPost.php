<!DOCTYPE html>

<form method="POST">
<p>Edit your story's title here!</p>
<textarea name ="editTitle"></textarea>
<p>Edit your story's link here!</p>
<<textarea name = "editLink"></textarea>
<input type = "submit" name="editPost" value="submit"/>
</form>
</body>

<?php

session_start();
if(!isset($_POST['editTitle'])){
	$_SESSION['story_id']=$_POST['post_id'];
}
if(isset($_POST['editTitle'])&&(isset($_POST['editLink']))&&(isset($_POST['editPost']))){

$editPost = $_POST['editPost'];
$editLink = $_POST['editLink'];
$editTitle = $_POST['editTitle'];

require "Database.php";

$stmt = $mysqli->prepare("update stories SET story_link = ?, title = ? where story_id = ?");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
 
$stmt->bind_param('ssi', $editLink, $editTitle, $_SESSION['story_id']);
 
$stmt->execute();
 
$stmt->close();
header("Location: guide.php");

}
?>

