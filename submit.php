<!DOCTYPE html>
<?php session_start();?>
<head>
<title> Submission Post </title>
</head>
<body>
<div id="container">
<span class="title">Submit Post</span>

<!--creates form that makes login terminal-->
<form method="POST">
<input type="text" name="postName" value="Post Name" /> <br>
<input type="text" name="postURL" value="Link" /> <br>
<input type="submit" name="postSubmitter" value="Submit Post"/> <br>
</form>
</div>
<?php
if(isset($_POST['postURL']) AND isset($_POST['postName']) AND isset($_POST['postSubmitter']))
{
	require "Database.php";
	$postName = $_POST['postName'];
	$postURL = $_POST['postURL'];
	$user_id = $_SESSION['user'];
	

	if($postURL=="" OR $postName=="")
	{
		echo "NO blank spaces ever, please";
	}
	if(filter_var($postURL, FILTER_VALIDATE_URL)==FALSE)
	{
		echo "Please enter a valid and correct URL";
	}
	else
	{
		$a = '10';
		$stmt = $mysqli->prepare("INSERT INTO stories (title, story_link, user_id) VALUES (?, ?, ?)");
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		$stmt->bind_param('ssi', $postName, $postURL, $user_id);   
		$stmt->execute();
		printf("%d", $mysqli->affected_rows);
		$stmt->close();
	}
	header("Location: guide.php");
}
?>
</body>
</html>
