<!DOCTYPE html>
<?php session_start();?>
<head>
<title> Submission Post </title>
</head>
<body>
<div id="container">
<span class="title">Submit Post</span>

<!--creates form that makes login terminal-->
<form method="POST">
<input type="text" name="postName" value="Post Name" /> <br>
<input type="text" name="postURL" value="Link" /> <br>
<input type="submit" name="postSubmitter" value="Submit Post"/> <br>
</form>
</div>
    
<?php
require "Database.php";
if(!isset($_POST['submitStory'])){
	if(isset($_GET["post_id"])){
		$_SESSION['post_id'] = $_GET["post_id"];
	}
	elseif (isset($_POST["post_id"])){
		$_SESSION['post_id'] = $_POST["post_id"];
	}
	loadComment();
}
function loadComment() {
	require "Database.php";
	$stmt2 = $mysqli->prepare("SELECT u.username, u.user_id, s.title, s.story_id FROM stories c JOIN users u ON c.user_id=u.user_id WHERE c.story_id = ?");
	if(!$stmt2){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt2->bind_param ('i', $_SESSION['post_id']);
	$stmt2->execute();
	$stmt2->bind_result($username, $user_id, $title, $story_id);
	
	while($stmt2->fetch()){
		echo $username.": ".$title.'<br>';
	}	
}

if(isset($_POST['submitComment'])){
	$post_id = $_SESSION['post_id'];
	$link = $_POST['postURL'];
        if($commentContent == "") {
            echo "No BLANK COMMENTS allowed";
        }
        else {
            $stmt = $mysqli->prepare("INSERT INTO comments (user_id, story_id, comment) VALUES (?, ?, ?)");
            if(!$stmt){
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
            }
	    $stmt->bind_param ('iis', $_SESSION['user'], $post_id, $commentContent);
            $stmt->execute();
            printf("%d", $mysqli->affected_rows);
            $stmt->close();
        }

	loadComment();
}

?>
</html>

